#!/usr/bin/env groovy

library identifier: 'jenkins-shared-library@main', retriever: modernSCM(
    [$class: 'GitSCMSource',
    remote: 'https://gitlab.com/mailo.shawn/jenkins-shared-library.git',
    credentialsID: 'gitlab-credentials'
    ]
)

pipeline {
    agent any
    tools {
        nodejs 'my-nodejs'
    }
    stages {
        stage('branch name'){
            steps {
                script {
                    echo "Branch Name = $BRANCH_NAME"
                }
            }
        }
        stage('increment version') {
            when {
                expression {
                    BRANCH_NAME == "main"
                }
            }
            steps {
                script {
                    dir('app') {
                        echo 'incrementing app version...'
                        sh 'npm version minor'
                        def packageJson = readJSON file: 'package.json'
                        def version = packageJson.version
                        env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                        env.DOCKERHUB_IMAGE_NAME = "chonzao/nodejs-app:${IMAGE_NAME}"
                    }
                }
            }
        }
        stage('run tests') {
            steps {
                script {
                    dir('app') {
                        echo 'running unit tests...'
                        sh 'npm install'
                        sh 'npm test'
                    }
                }
            }
        }
        stage('build and push docker image') {
            when {
                expression {
                    BRANCH_NAME == "main"
                }
            }
            steps {
                script {
                    dir('app') {
                        echo 'building the docker image...'
                        buildImage(env.DOCKERHUB_IMAGE_NAME)
                        dockerLogin()
                        dockerPush(env.DOCKERHUB_IMAGE_NAME)
                    }
                }
            }
        } 
        stage("deploy") {
            when {
                expression {
                    BRANCH_NAME == "main"
                }
            }
            steps {
                script {
                    dir('app') {
                        echo 'deploying docker image to EC2...'

                        def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
                        def ec2Instance = "ec2-user@54.176.129.10"

                        sshagent(['ec2-server-key']) {
                            sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                            sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
                            sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
                        }
                    }
                }
            }               
        }
        stage('commit version update'){
            when {
                expression {
                    BRANCH_NAME == "main"
                }
            }
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
                        sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/mailo.shawn/aws-nodejs-app.git'
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}
